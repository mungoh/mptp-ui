describe('MPTP homepage', () => {
	beforeEach(() => {
		cy.visit('/')
	})

	it('has the correct <h1>', () => {
		cy.contains('h1', 'Most Past the Post')
	})

	it('navigates to /how-it-works', () => {
		cy.get('nav a').contains('How it works').click()
		cy.url().should('include', '/how-it-works')
	})

	it('navigates to /demo', () => {
		cy.get('nav a').contains('Demo').click()
		cy.url().should('include', '/demo/gb-general-election-2017/dataset')
	})

	it('switches elections', () => {
		cy.get('.election-selector')
			.select('2015')
			.should('have.value', 'gb-general-election-2015')
	})

	it.skip('toggles pie chart display', () => {
		cy.get('.pie-chart-view')
			.click()
		// click twice spec fix - seems to be a bit flakey
		cy.get('.pie-chart-view')
			.click()
		
		cy.get('.pie-chart-view')
			.should('have.class', 'active')

		cy.get('.pie-chart')
			.should('be.visible')
		  .should('have.length', 2)
	})

	it.skip('toggles bar chart display', () => {
		cy.get('.bar-chart-view')
			.click()
		// click twice spec fix - seems to be a bit flakey
		cy.get('.bar-chart-view')
			.click()

		cy.get('.bar-chart-view')
			.should('have.class', 'active')

		cy.get('.bar-chart')
			.should('be.visible')
		  .should('have.length', 2)
	})
})