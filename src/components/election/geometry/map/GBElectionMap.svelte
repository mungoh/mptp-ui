<script>
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import has from 'lodash/has'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import { onMount } from 'svelte'
import { mapData, electionResultsLoaded, mapDataLoaded } from '../../../../stores/elections.js'
import { pinCoordinates, cellDistance } from '../../../../stores/map.js'
import SeatHexagon from '../SeatHexagon.svelte'

export let seats = []
export let geometry = false
// export let debug = false

let regionPaths = []
$: regionPaths = map(get($mapData, 'region-paths', {}), (path, slug) => {
  return {
    'data-region': slug, 
    d: path,
    stroke: '#000000',
    fill: 'none',
    'stroke-width': 1
  }
})

let borderAttributes = { stroke: '#000000', fill: 'none', 'stroke-width': 1 }
let regions = []
$: {
  regions = map(get($mapData, 'regions', {}), (constituencies, slug) => {
    return {
      slug: slug,
      constituencies: constituencies
    }
  })
}

let parties = {}
$: {
  if (isEmpty(seats)) {
    parties = {}
  } else {
    parties = {}
    forEach(seats, (seat) => {
      const constituency = get(seat, 'constituency.slug')
      parties[constituency] = get(seat, 'party')
    })
  }
}

let mapContainer
$:{
  if (geometry && mapContainer) {
    if (!$pinCoordinates) {
      const mapBox = mapContainer.getBoundingClientRect()
      pinCoordinates.set({
        x: mapBox.x + (mapBox.width / 2),
        y: mapBox.y + (mapBox.height / 2)
      })
    }
    if (!$cellDistance) {
      const mapCell = mapContainer.querySelector('polygon[data-constituency=isle-of-wight]')
      const cellBox = mapCell.getBoundingClientRect()
      cellDistance.set(cellBox.width)
    }
  }
}
</script>

{#if !$electionResultsLoaded || !$mapDataLoaded}
  <div class="loading"></div>
{:else}
  <svg
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g class="map-container" bind:this={mapContainer}>
      <g class="region-borders">
        {#each regionPaths as pathAttributes}
          <path {...pathAttributes} />
        {/each}
      </g>

      {#each regions as {constituencies, slug}}
        <g class="border {slug}" {...borderAttributes}>
          {#each constituencies as constituency, idx}
            {#if has(parties, constituency.slug)}
              <SeatHexagon on:select-constituency constituency={constituency} party={parties[constituency.slug]} />
            {:else}
              <SeatHexagon on:select-constituency constituency={constituency} />
            {/if}
          {/each}
        </g>
      {/each}
      <!-- 
      {#if debug && geometry && $pinCoordinates}
        <circle cx="{$pinCoordinates.x}" cy="{$pinCoordinates.y}" r="20" fill="red" stroke="yellow" stroke-width="2" />
      {/if} -->
    </g>
  </svg>
{/if}
