export default function (polygon) {
  if (!polygon) {
    return
  }
  const pointsList = polygon.points
  if (!pointsList) {
    return
  }

  const center = {
    x: 0,
    y: 0
  }
  
  for (let i = 0; i < pointsList.length; i++) {
    const point = pointsList[i]

    center.x += (point.x / pointsList.length)
    center.y += (point.y / pointsList.length)
  }

  return center
}
