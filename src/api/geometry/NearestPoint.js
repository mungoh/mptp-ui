import _ from 'lodash'
import { quadtree } from 'd3-quadtree'
import PolygonCenter from './PolygonCenter.js'

function midPoint(points) {
  const xVals = _.map(points, 'x')
  const yVals = _.map(points, 'y')

  const minX = _.min(xVals)
  const maxX = _.max(xVals)
  const minY = _.min(yVals)
  const maxY = _.max(yVals)

  var midX = minX + ((maxX - minX) * 0.50)
  var midY = minY + ((maxY - minY) * 0.50)

  return {
    x: midX,
    y: midY
  }
}

// mid-point of the polygon path in the "compass" direction
function boundaryMidpoint (polygon, direction) {
  const xSortedPoints = _.sortBy(polygon.points, 'x')
  const ySortedPoints = _.sortBy(polygon.points, 'y')
  const east = _.last(xSortedPoints)
  const west = _.first(xSortedPoints)
  const northerlies = _.sortBy(_.slice(ySortedPoints, 0, 2), 'x') // lowest 2 y points
  const southerlies = _.sortBy(_.slice(ySortedPoints, ySortedPoints.length - 2), 'x') // highest 2 y points

  let dirPoints
  switch (direction) {
    case 'north' :
      dirPoints = northerlies
      break
    case 'north-east' :
      dirPoints = [_.last(northerlies), east]
      break
    case 'north-west' :
      dirPoints = [west, _.first(northerlies)]
      break
    case 'south-east' :
      dirPoints = [_.first(southerlies), east]
      break
    case 'south-west' :
      dirPoints = [west, _.first(southerlies)]
      break
    case 'south' :
      dirPoints = southerlies
      break
  }

  return midPoint(dirPoints)
}

function exclusionLine (polygon, direction) {
  const xSortedPoints = _.sortBy(polygon.points, 'x')
  const ySortedPoints = _.sortBy(polygon.points, 'y')
  const east = _.last(xSortedPoints)
  const west = _.first(xSortedPoints)
  const northerlies = _.sortBy(_.slice(ySortedPoints, 0, 2), 'x') // lowest 2 y points
  const southerlies = _.sortBy(_.slice(ySortedPoints, ySortedPoints.length - 2), 'x') // highest 2 y points

  switch (direction) {
    case 'north' :
      return [west, east]
    case 'north-east' :
      return [_.first(northerlies), _.last(southerlies)]
    case 'north-west' :
      return [_.first(southerlies), _.last(northerlies)]
    case 'south-east' :
        return [_.last(northerlies), _.first(southerlies)]
    case 'south-west' :
        return [_.last(southerlies), _.first(northerlies)]
    case 'south' :
      return [east, west]
  }
}

function isLeftOfLine (linePoints, targetPoint) {
  return ((linePoints[1].x - linePoints[0].x) * (targetPoint.y - linePoints[0].y) - (linePoints[1].y - linePoints[0].y) * (targetPoint.x - linePoints[0].x)) <= 0
}

function filterPoints(from, polygon, coordinates, direction) {
  let exclLine
  if (direction) {
    exclLine = exclusionLine(polygon, direction)
  }

  const targets = _.filter(
    _.filter(coordinates, (coords) => { return coords.x !== from.x || coords.y !== from.y }),
    (coords) => { 
      return !exclLine || isLeftOfLine(exclLine, coords) }
  )
  
  return targets
}

export default function (from, fromPolygon, coordinates, direction = null) {
  const width = _.max(coordinates, 'x')
  const height = _.max(coordinates, 'y')
  let targets
  
  let srcPoint
  if (fromPolygon && direction) {
    targets = filterPoints(from, fromPolygon, coordinates, direction)
    srcPoint = boundaryMidpoint(fromPolygon, direction)
  } else {
    targets = coordinates
    srcPoint = from
  }
  const points = _.map(targets, (coords) => [coords.x, coords.y])

  let qtree = quadtree()
    .extent([[-1, -1], [width + 1, height + 1]])
    .addAll(points)

  const nearestPoint = qtree.find(srcPoint.x, srcPoint.y)
  if (!nearestPoint) {
    return null
  }

  return _.find(coordinates, (coord) => {
    return coord.x === nearestPoint[0] && coord.y === nearestPoint[1]
  })
}
