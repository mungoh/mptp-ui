import _ from 'lodash'

export const sampleParties = [{
    slug: 'party-one',
    name: 'Party One',
    label: 'Party One',
    num_seats: 230,
    batchSize: 8,
    rankings: [],
    numAssigned: 0
  },
  {
    slug: 'party-two',
    name: 'Party Two',
    label: 'Party Two',
    num_seats: 214,
    batchSize: 7,
    rankings: [],
    numAssigned: 0
  },
  {
    slug: 'party-three',
    name: 'Party Three',
    label: 'Party Three',
    num_seats: 65,
    batchSize: 2,
    rankings: [],
    numAssigned: 0
  },
  {
    slug: 'party-four',
    name: 'Party Four',
    label: 'Party Four',
    num_seats: 52,
    batchSize: 2,
    rankings: [],
    numAssigned: 0
  },
  {
    slug: 'party-five',
    name: 'Party Five',
    label: 'Party Five',
    num_seats: 29,
    batchSize: 1,
    rankings: [],
    numAssigned: 0
  }
]

const currentPartyBatch = function (availableConstituencies, partyRankings, batchSize) {
  let count = 0
  const targetBatch = _.filter(partyRankings, (ranking) => {
    if (count >= batchSize) {
      return false
    }
    const idx = _.findIndex(availableConstituencies, function(c) { return c.slug === ranking.constituency.slug })
    if (idx >= 0) {
      count += 1
      return true
    }
    return false
  })
  return _.map(targetBatch, 'constituency')
}

export const generateResultsConstituenciesSample = function (constituencies) {
  const numConstituencies = _.sumBy(sampleParties, 'num_seats')
  const sourceConstituencies = _.values(constituencies)
  return _.sampleSize(sourceConstituencies, numConstituencies)
}

const generatePartyRankings = function (constituencies) {
  let demoParties = _.cloneDeep(sampleParties)
  _.times(demoParties.length, (n) => {
    demoParties[n].rankings = _.map(_.shuffle(constituencies), (c) => {
      return {
        constituency: c
      }
    })
  })
  return demoParties
}

const generateCalculations = function (constituencies, parties) {
  let availableConstituencies = _.cloneDeep(constituencies)
  let calculations = []
  let currentBatch
  let calculation
  let orderSeq = 0

  let batchTargets = {}

  let batchTarget
  let batchSize

  if (_.isEmpty(parties)) {
    return []
  }

  let allocationsComplete = false
  while (!allocationsComplete) {
    calculation = {
      order_seq: orderSeq,
      seats: []
    }
    _.forEach(parties, (party) => {
      batchTarget = _.get(batchTargets, party.slug, 0)
      batchSize = _.min([party.batchSize, (party.num_seats - batchTarget)])
      currentBatch = currentPartyBatch(availableConstituencies, party.rankings, batchSize)
      _.forEach(currentBatch, (constituency) => {
        calculation.seats.push({
          constituency: constituency,
          party: party
        })
        _.remove(availableConstituencies, (c) => {
          return c.slug === constituency.slug
        })
      })
      batchTargets[party.slug] = batchTarget + batchSize
    })
    calculations.push(calculation)
    orderSeq += 1

    allocationsComplete = _.every(batchTargets, (t, slug) => {
      const targetParty = _.find(parties, {
        slug: slug
      })
      return t >= targetParty.num_seats
    })
  }

  return calculations
}

export default {
  sampleParties,
  generatePartyRankings,
  generateCalculations,
  generateResultsConstituenciesSample
}
