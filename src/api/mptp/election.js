import Axios from 'axios'
import fetch from 'node-fetch'
import first from 'lodash/first'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

const APP_ELECTIONS_DATA_ROOT = `${process.env.APP_SERVER}/data/gb`

function findAll() {
  return process.env.APP_ELECTIONS_INDEX
}

function findElection(id) {
  const url = `${APP_ELECTIONS_DATA_ROOT}/elections/${id}.json`
  return fetch(url).then((response) => response.json())
}

function findFirstPastPostResults(electionId) {
  const url = `${APP_ELECTIONS_DATA_ROOT}/elections/${electionId}/results/fpp.json`
  return fetch(url).then((response) => response.json())
}

function findMostPastPostResults(electionId) {
  const url = `${APP_ELECTIONS_DATA_ROOT}/elections/${electionId}/results/mpp.json`
  return fetch(url).then((response) => response.json())
}

function loadResults(electionId) {
  return Axios.all([findFirstPastPostResults(electionId), findMostPastPostResults(electionId)])
    .then(Axios.spread(function (fppResults, mppResults) {
      return {
        fpp_results: fppResults,
        mpp_results: mppResults
      }
    }))
}

function loadCalculations(electionId) {
  const url = `${APP_ELECTIONS_DATA_ROOT}/elections/${electionId}/most-past-post/calculations.json`
  return Axios.get(url)
    .then((response) => {
      const data = response.data

      const fppCalculation = first(data)

      forEach(fppCalculation.seats, (seat) => {
        // TODO: temp fix for missing party name (speaker)
        if (isEmpty(get(seat, 'party.abbreviation'))) {
          seat.party = {
            abbreviation: 'speaker',
            shortName: 'The Speaker'
          }
        }
      })
      data[0] = fppCalculation

      return data
    })
}

function loadParties(electionId) {
  const url = `${APP_ELECTIONS_DATA_ROOT}/elections/${electionId}/most-past-post/parties.json`
  return fetch(url).then((response) => response.json())
}

function loadPartyRankings(electionId) {
  const url = `${APP_ELECTIONS_DATA_ROOT}/elections/${electionId}/most-past-post/party-rankings.json`
  return fetch(url).then((response) => response.json())
                   .then((data) => {
                    return map(data, (p) => {
                      p.rankings = p.ranked_candidates
                      return p
                    })
                   })
}

function loadMapData(year) {
  let mapFile = `map-data-${year}.json`
  if (year >= 2010) {
    mapFile = 'map-data-latest.json'
  }
  const url = `${APP_ELECTIONS_DATA_ROOT}/geo/${mapFile}`
  return fetch(url).then((response) => response.json())
}

export default {
  findAll,
  findElection,
  loadResults,
  loadCalculations,
  loadMapData,
  loadParties,
  loadPartyRankings
}
