import _filter from 'lodash/filter'
import _get from 'lodash/get'
import _includes from 'lodash/includes'
import _map from 'lodash/map'
import _size from 'lodash/size'
import _union from 'lodash/union'

import {
  writable,
  derived,
  get
} from 'svelte/store'

function createElectionConstituencies () {
  const {
    subscribe,
    set,
  } = writable(null)

  return {
    subscribe,
    set
  }
}
export const constituencies = createElectionConstituencies()

function createResultsConstituencies () {
  const {
    subscribe,
    set,
  } = writable(null)

  return {
    subscribe,
    set
  }
}
export const resultsConstituencies = createResultsConstituencies()

function createAssignedConstituencies () {
  const {
    subscribe,
    set
  } = writable([]);
  return {
    subscribe,
    set,
    update: (payload) => {
      let replacements = get(assignedConstituencies)
      set(_union(replacements, payload))
    },
    reset: () => set([])
  }
}
export const assignedConstituencies = createAssignedConstituencies()
export const assignedConstituencySlugs = derived(
  assignedConstituencies,
  $assignedConstituencies => _map($assignedConstituencies, 'slug')
)
export const numAssignedConstituencies = derived(
  assignedConstituencies,
  $assignedConstituencies => _size($assignedConstituencies)
)

export const unassignedConstituencies = derived(
  [resultsConstituencies, assignedConstituencySlugs],
  ([$resultsConstituencies, $assignedConstituencySlugs]) => _filter($resultsConstituencies, (c) => {
    return !_includes($assignedConstituencySlugs, _get(c, 'slug'))
  })
)

export const unAssignedConstituencySlugs = derived(
  unassignedConstituencies,
  $unassignedConstituencies => _map($unassignedConstituencies, 'slug')
)
