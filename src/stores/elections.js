import {
  writable,
  readable,
  derived
} from 'svelte/store'
import _ from 'lodash'
import { constituencies, resultsConstituencies } from './constituencies.js'
import { parties, partyRankings } from './parties.js'
import Election from '../api/mptp/election.js'

export const elections = readable([], function start(set) {
  set(Election.findAll())

  return function stop() {}
})

export const electionsLoaded = derived(
  elections,
  $elections => !_.isEmpty($elections)
)

export const electionIds = derived(
  elections,
  $elections => _.reverse(_.map($elections, 'id'))
)

export const pendingElectionIds = derived(
  elections,
  $elections => _.reverse(
    _.map(
      _.filter($elections, (elec) => !_.has(elec, 'results.most_past_post_result.id')),
      'id'
    )
  )
)

export const defaultElectionId = derived(
  [elections, pendingElectionIds],
  ([$elections, $pendingElectionIds], set) => {
    const defaultId = _.get(_.find($elections, (elec) => !_.includes($pendingElectionIds, elec.id)), 'id')
    set(defaultId)
  }
)

function createElectionResults () {
  const {
    subscribe,
    set,
  } = writable(null);

  return {
    subscribe,
    load: (electionId) => {
      Election.loadResults(electionId)
        .then((data) => {
          set(data)

          const electionConstituencies = _.map(_.get(data, 'fpp_results.seats', []), 'constituency')
          constituencies.set(electionConstituencies)
          resultsConstituencies.set(electionConstituencies)
        })
    },
    loadParties: (electionId) => {
      Election.loadParties(electionId)
        .then((apiParties) => {
          parties.set(apiParties)
        })

      Election.loadPartyRankings(electionId)
        .then((apiPartyRankings) => {
          partyRankings.set(apiPartyRankings)
        })
    },
    reset: () => {
      parties.set([])
      resultsConstituencies.set(null)
      set(null)
    }
  }
}

export const electionResults = createElectionResults()
export const electionResultsLoaded = derived(
  electionResults,
  $electionResults => !_.isEmpty($electionResults)
)

function createMapData() {
  const {
    subscribe,
    set,
  } = writable(null);

  return {
    subscribe,
    load: (year) => {
      set(null)
      Election.loadMapData(year)
        .then((data) => { set(data) })
    },
    reset: () => set(null)
  }
}

export const mapData = createMapData()
export const mapDataLoaded = derived(
  mapData,
  $mapData => !_.isEmpty($mapData)
)

function createElection() {
  const {
    subscribe,
    set,
  } = writable(null);

  return {
    subscribe,
    load: (id) => {
      mapData.reset()
      electionResults.reset()
      Election.findElection(id).then((elec) => {
        set(elec)
        mapData.load(elec.year)
        electionResults.load(id)
      })
      .catch((e) => {
        console.error(`error ${e}`)
        set(null)
      })
    },
    reset: () => {
      mapData.reset()
      electionResults.reset()
      set(null)
    }
  }
}

export const election = createElection()

export const electionLoaded = derived(
  election,
  $election => !_.isEmpty($election)
)
