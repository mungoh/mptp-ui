import { writable } from 'svelte/store'

export const coordinates = writable(null)
export const pinCoordinates = writable(null)
export const cellDistance = writable(null)
export const highlightCell = writable(null)
