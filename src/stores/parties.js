import {
  writable,
  derived,
  get
} from 'svelte/store'
import _findIndex from 'lodash/findIndex'
import _get from 'lodash/get'
import _forEach from 'lodash/forEach'
import _map from 'lodash/map'
import _max from 'lodash/max'
import _maxBy from 'lodash/maxBy'
import _set from 'lodash/set'
import _size from 'lodash/size'
import _union from 'lodash/union'

function createParties() {
  const {
    subscribe,
    set
  } = writable([])

  return {
    subscribe,
    set,
    updateAssigned: (payload) => {
      let replacements = get(parties)
      _forEach(payload, (constituencySlugs, slug) => {
        let idx = _findIndex(replacements, { slug: slug })
        if (idx < 0) {
          return
        }

        const party = _get(replacements, `[${idx}]`)
        const merged = _union(_get(party, 'assigned', []), constituencySlugs)
        _set(party, 'assigned', merged)
        _set(party, 'numAssigned', _size(merged))
        _set(replacements, idx, party)
      })
      set(replacements)
    },
    resetAssignments: () => {
      let replacements = get(parties)
      _forEach(replacements, (party, idx) => {
        _set(party, 'assigned', [])
        _set(party, 'numAssigned', 0)
        _set(replacements, idx, party)
      })
      set(replacements)
    }
  }
}
export const parties = createParties()

function createPartyRankings() {
  const {
    subscribe,
    set,
  } = writable(null)

  return {
    subscribe,
    set
  }
}
export const partyRankings = createPartyRankings()


export const partiesLoaded = derived(
  [parties],
  ([$parties]) => _size($parties) > 0
)

export const partyRankingsLoaded = derived(
  [partyRankings],
  ([$partyRankings]) => _size($partyRankings) > 0
)

export const partyAssignments = derived(
  [parties],
  ([$parties]) => {
    return _map($parties, (p) => { return { slug: p.slug, assigned: p.assigned } })
  }
)

export const maxPartyBatchSize = derived(
  [parties],
  ([$parties]) => {
    const chunkiest = _maxBy($parties, (p) => { return _get(p, 'batchSize', 0) })
    return chunkiest.batchSize
  }
)
