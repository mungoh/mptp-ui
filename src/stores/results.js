import {
  writable,
  derived,
  get
} from 'svelte/store'
import _ from 'lodash'

import {
  parties
} from './parties.js'

import {
  assignedConstituencies
} from './constituencies.js'


export const calculationComplete = writable(false)
function createResultsCalculations() {
  const {
    subscribe,
    set,
  } = writable([])

  return {
    subscribe,
    set,
    reset: () => {
      calculationIndex.set(null)
      parties.resetAssignments()
      assignedConstituencies.reset()
      calculationComplete.set(false)
      set(null)
    }
  }
}
export const resultsCalculations = createResultsCalculations()
export const numCalculations = derived(
  resultsCalculations,
  $resultsCalculations => _.size($resultsCalculations)
)

function createCalculationIndex() {
  const {
    subscribe,
    set
  } = writable(null)

  return {
    subscribe,
    set,
    loadNext: () => {
      const currentIdx = get(calculationIndex)
      if (!_.isInteger(currentIdx)) {
        set(0)
      } else {
        const nextIdx = currentIdx + 1
        if (nextIdx < _.size(get(resultsCalculations))) {
          set(nextIdx)
        } else {
          calculationComplete.set(true)
        }
      }
    },
    process: (idx) => {
      const seats = get(resultsCalculations)[idx].seats
      let constituenciesPayload = []
      const partiesPayload = {}
      
      _.forEach(seats, (seat) => {
        const partySlug = seat.party.slug
        if (_.isEmpty(partiesPayload[partySlug])) {
          partiesPayload[partySlug] = []
        }
        partiesPayload[partySlug].push(seat.constituency.slug)

        constituenciesPayload.push({ slug: seat.constituency.slug, party: partySlug })
      })
      if (_.size(constituenciesPayload) > 0) {
        assignedConstituencies.update(constituenciesPayload)
      }
      if (_.size(partiesPayload) > 0) {
        parties.updateAssigned(partiesPayload)
      }

      if (idx >= _.size(get(resultsCalculations)) - 1) {
        calculationComplete.set(true)
      } else {
        set(idx + 1)
      }
    }
  }
}
export const calculationIndex = createCalculationIndex()

export const currentCalculation = derived([resultsCalculations, calculationIndex], ([$resultsCalculations, $calculationIndex]) => {
  if (!_.isInteger($calculationIndex)) {
    return null
  }
  const current = _.get($resultsCalculations, $calculationIndex)
  return current
})

export const currentCalculationHighlights = derived(
  [currentCalculation],
  ([$currentCalculation]) => {
    if (!$currentCalculation) {
      return null
    }
    const seats = _.get($currentCalculation, 'seats', [])
    const highlights = {}
    _.forEach(seats, (seat) => {
      if (!_.has(highlights, seat.party.slug)) {
        highlights[seat.party.slug] = []
      }
      highlights[seat.party.slug].push(seat.constituency.slug)
    })

    return highlights
  }
)

export const currentCalculationConstituencies = derived(
  [currentCalculation],
  ([$currentCalculation]) => {
    if (!$currentCalculation) {
      return null
    }
    const seats = _.get($currentCalculation, 'seats', [])
    const highlights = {}
    _.forEach(seats, (seat) => {
      highlights[seat.constituency.slug] = seat.party.slug
    })

    return highlights
  }
)

export const animationSpeed = writable(0)
