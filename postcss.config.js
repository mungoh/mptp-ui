// const production = !process.env.ROLLUP_WATCH
// const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
  plugins: [
    require('postcss-import')(),
    require('tailwindcss')('./src/style/tailwind.config.js'),
    require('autoprefixer'),
    // Only purge css on production
    // production && purgecss({
    //   content: ["./**/*.html", "./**/*.svelte"],
    //   whitelistPatterns: [/(w-1\/[0-9])$/],
    //   defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
    // })
  ]
}
