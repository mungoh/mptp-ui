# mptp UI

This is the frontend project for [Most Past the Post (MPTP)](https://mostpastpost.info). It's built on the svelte/sapper framework and relies on data generated and exported from the MPTP engine project (source code not yet public).

Most Past the Post is a proposal to reform the First Past the Post voting system. It applies a set of calculations to first past the post election results, reassigning some seats to losing candidates in order to achieve proportional representation.

## Get started

Install the dependencies...

```bash
cd mptp-ui
yarn install
```

...then start:
```bash
yarn dev
```
Navigate to [localhost:5000](http://localhost:3000). You should see the frontend website.

## Datasets
Currently the MPTP engine exports UK general election data for years where it has been collated and processed. The exports are committed into this respository in order for the frontend to be self-contained (see the static/data/gb directory).
